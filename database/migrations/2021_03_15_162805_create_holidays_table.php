<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHolidaysTable extends Migration
{
    public function up()
    {
        Schema::create('holidays', function (Blueprint $table) {
            $table->id();
            $table->timestamps();

            $table->string('code')->unique();
            $table->string('name');
            $table->string('destination');

            $table->integer('guests');
            $table->integer('monis');

            $table->dateTime('leaves_at')->index();
            $table->dateTime('returns_at')->index();
            $table->string('departure_location')->default('');
            $table->string('return_location')->default('');
        });
    }

    public function down()
    {
        Schema::dropIfExists('holidays');
    }
}

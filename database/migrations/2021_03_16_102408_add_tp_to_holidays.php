<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTpToHolidays extends Migration
{
    public function up()
    {
        Schema::table('holidays', function (Blueprint $table) {
            $table->integer('tp')->default(0);
        });
    }

    public function down()
    {
        Schema::table('holidays', function (Blueprint $table) {
            $table->dropColumn('tp');
        });
    }
}

@extends('errors::minimal')

@section('title', "Onderhoud")
@section('code', '503')
@section('message')
    <p>
        Oeps! Onze site is even offline voor onderhoud,
        zo dadelijk komt deze terug met nog meer functies dan hiervoor!
    </p>
    <p>
        Tot zo!
    </p>
@endsection


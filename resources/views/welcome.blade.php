@extends('layouts.app')

@section('title', 'Vakantietracker')

@section('head')
    <meta http-equiv="refresh" content="3600">
@endsection

@section('content')
    {{-- Facebook SDK, I'm sorry --}}
    @if ($show_facebook)
        <div id="fb-root"></div>
        <script async defer crossorigin="anonymous"
                src="https://connect.facebook.net/nl_BE/sdk.js#xfbml=1&version=v10.0&appId=230607273739192"
                nonce="z2jy7AJT"></script>
    @endif

    <div class="w-full p-2">
        <div class="w-full p-2 text-center">
            <img src="{{ asset('img/logo.png') }}" alt="Green van logo" class="h-7 md:h-11 my-auto w-auto inline-block align-baseline">
            <h1 class="inline-block font-bold text-green-500 text-3xl md:text-5xl">Vakantietracker</h1>
        </div>
        <div class="flex flex-col md:flex-row flex-wrap mb-4">
            <livewire:guests-count/>
            <livewire:holiday-count/>
            <livewire:volunteer-count/>
        </div>
        <div class="flex flex-col md:flex-row flex-wrap">
            <div class="flex-grow flex flex-col mb-4">
                <livewire:departures-and-arrivals/>
                @if ($show_facebook)
                    <div class="m-2 text-center">
                        <div class="fb-page" data-href="https://www.facebook.com/kazouovl" data-tabs="timeline"
                             data-width="500" data-height="600" data-small-header="true"
                             data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true">
                            <blockquote cite="https://www.facebook.com/kazouovl" class="fb-xfbml-parse-ignore">
                                <a href="https://www.facebook.com/kazouovl">Kazou Oost-Vlaanderen</a>
                            </blockquote>
                        </div>
                    </div>
                @endif
            </div>
            @if ($show_instagram)
                <div class="flex-grow m-2 mb-4 max-h-screen" style="flex-basis: 50%;">
                    <script src='https://embedsocial.com/js/iframe.js'></script>
                    <iframe style='border: 0; width: 100%; height: 100%;' scrolling=no
                            src='https://embedsocial.com/facebook_album/pro_hashtag/117cf255907b167438fdbce7edb9e015e4fc198c'></iframe>
                    <script>iFrameResize();</script>
                </div>
            @endif
        </div>
    </div>
@endsection

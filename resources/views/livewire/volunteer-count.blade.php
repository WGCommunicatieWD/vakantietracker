<div class="counter-card" wire:poll.30s>
    <div class="font-bold text-5xl md:text-9xl">{{ $moni_count + $tp_count }}</div>
    <div class="text-lg md:text-xl">fantastische vrijwilligers, <b>{{ $moni_count }}</b> monitoren en <b>{{ $tp_count }}</b> TP.</div>
</div>

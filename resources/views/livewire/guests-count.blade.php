<div class="counter-card" wire:poll.30s>
    <div class="font-bold text-5xl md:text-9xl">{{ number_format($count, 0, ',', ' ') }}</div>
    <div class="text-lg md:text-xl">deelnemers blij gemaakt!</div>
</div>

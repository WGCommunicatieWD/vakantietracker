<div class="counter-card" wire:poll.30s>
    <div class="font-bold text-5xl md:text-9xl">{{ $count_left }}</div>
    <div class="text-lg md:text-xl">vakanties vertrokken, al <b>{{ $count_returned }}</b> terug.</div>
</div>

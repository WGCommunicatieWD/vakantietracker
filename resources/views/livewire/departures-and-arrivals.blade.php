<div wire:poll.30s>
    <table class="departures-arrivals mb-4">
        <thead>
        <tr>
            <th scope="col" colspan="4">Vakanties Vandaag</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($holidays as $holiday)
            <tr>
                @if ($holiday->departs)
                    <td class="departure-label">
                        <svg>
                            <use xlink:href="{{ asset('img/sprite.min.svg') }}#chevron-right"/>
                        </svg>
                    </td>
                @else
                    <td class="return-label">
                        <svg>
                            <use xlink:href="{{ asset('img/sprite.min.svg') }}#chevron-left"/>
                        </svg>
                    </td>
                @endif

                <td>{{ $holiday->name }}</td>
                <td>{{ $holiday->ages }}</td>
                <td>{{ $holiday->destination }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>

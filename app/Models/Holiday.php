<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Holiday extends Model
{
    use HasFactory;

    protected $fillable = [
        'code', 'name', 'destination', 'guests', 'monis', 'ages', 'tp',
        'leaves_at', 'returns_at', 'departure_location', 'return_location',
    ];

    protected $casts = [
        'leaves_at' => 'datetime',
        'returns_at' => 'datetime',
    ];
}

<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class ImportHolidaysCsv extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'holidays:importcsv {file : Pad naar een csv bestand.}
{--wipe-first : Wis de hele lijst met vakanties eerst}
{--dry-run : Test de operaties uit, maar pas de database niet aan.}
{--t|fill-time=08u00 : De tijd om te gebruiken wanneer er geen staat in de CSV}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Importeer een csv bestand met vakanties in de database.
    Standaard worden bestaande vakanties overschreven.';

    protected $column_map = [
        'Vakantiecode' => 'code',
        'VakantieConcept Centrum' => 'destination',
        'Vakantieconcept' => 'name',
        'Leeftijdscluster' => 'ages',
        'Deelnemers' => 'guests',
        'Aantal deelnemers' => 'guests', // Alternative title
        'Monitoren' => 'monis',
        'TP' => 'tp',
        'Vertrekplaatsen' => 'departure_location',
        'Aankomstplaatsen' => 'return_location',
        'Startdatum' => 'leaves_at',
        'Einddatum' => 'returns_at',
    ];

    protected function decode_header(array $header): array
    {
        return array_map(function ($item) {
            foreach ($this->column_map as $import_name => $name) {
                if (str_contains($item, $import_name)) {
                    return $name;
                }
            }

            return ''; // This will get filtered out
        }, $header);
    }

    protected function split_location_time(string $combo): array
    {
        $matches = [];
        $res = preg_match('/(.*) \((\d{1,2}u\d{2})\*?(?:-\d{1,2}u\d{2}\*?)?\)(,.+)?/', $combo, $matches);
        if ($res == 0) {
            if (str_contains($combo, "eigen vervoer")) {
                $matches = [
                    1 => "Eigen vervoer",
                    2 => $this->option('fill-time'),
                ];
            } else {
                throw new \Exception("Could not extract location for string $combo");
            }
        }

        // Convert something like 8u15 or 08u15 into 08:15:00 robustly
        $time = Str::of($matches[2])
            ->explode('u')
            ->map(function (String $number) {
                return str_pad($number, 2, '0', STR_PAD_LEFT);
            })
            ->join(':') . ':00';

        return [
            'location' => $matches[1],
            // TODO Handle e.g. 8u15 instead of 08u15
            'timeslot' => $time,
        ];
    }

    protected function decode_row(array $decoded_header, array $row): array
    {
        $result = array_combine($decoded_header, $row);
        unset($result['']); // Remove redundant rows

        // Decode departure and return location and time
        $decoded_departure = $this->split_location_time($result['departure_location']);
        $result['departure_location'] = $decoded_departure['location'];
        $result['leaves_at'] = Carbon::parse($result['leaves_at'] . ' ' . $decoded_departure['timeslot'])->toDateTimeString();

        $decoded_return = $this->split_location_time($result['return_location']);
        $result['return_location'] = $decoded_return['location'];
        $result['returns_at'] = Carbon::parse($result['returns_at'] . ' ' . $decoded_return['timeslot'])->toDateTimeString();

        return $result;
    }

    public function __construct()
    {
        parent::__construct();
    }

    public function handle(): int
    {
        if (! file_exists($this->argument('file'))) {
            $this->error('Het opgegeven bestand bestaat niet.');
            return -1;
        }

        $handle = fopen($this->argument('file'), 'r');
        if ($handle === false) {
            $this->error("Kan het bestand niet openen.");
            return -1;
        }

        $dry_run = boolval($this->option('dry-run'));
        $wipe_first = boolval($this->option('wipe-first'));

        $holiday_info = [];
        $header = $this->decode_header(fgetcsv($handle));
        while ($row = fgetcsv($handle)) {
            $holiday_info[] = $this->decode_row($header, $row);
        }

        if ($dry_run) {
            dd($holiday_info);
        }

        DB::transaction(function () use ($holiday_info, $wipe_first) {
            if ($wipe_first) {
                DB::table('holidays')->delete();
            }

            DB::table('holidays')->upsert($holiday_info,
            ['code'], ['ages', 'name', 'destination', 'guests', 'monis', 'leaves_at', 'returns_at']);
        });

        return 0;
    }
}

<?php

namespace App\Http\Livewire;

use App\Models\Holiday;
use Livewire\Component;

class GuestsCount extends Component
{
    public $count = 0;

    public function mount(): void
    {
        $this->updateData();
    }

    public function updateData(): void
    {
        $this->count = Holiday::where('leaves_at', '<', now())->sum('guests');
    }

    public function render()
    {
        $this->updateData();
        return view('livewire.guests-count');
    }
}

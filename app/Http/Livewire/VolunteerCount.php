<?php

namespace App\Http\Livewire;

use App\Models\Holiday;
use Livewire\Component;

class VolunteerCount extends Component
{
    public $moni_count = 0;
    public $tp_count = 0;

    public function updateData(): void
    {
        $this->moni_count = Holiday::where('leaves_at', '<', now())->sum('monis');
        $this->tp_count = Holiday::where('leaves_at', '<', now())->sum('tp');
    }

    public function render()
    {
        $this->updateData();
        return view('livewire.volunteer-count');
    }
}

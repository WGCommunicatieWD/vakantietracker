<?php

namespace App\Http\Livewire;

use App\Models\Holiday;
use Livewire\Component;

class HolidayCount extends Component
{
    public $count_left = 0;
    public $count_returned = 0;

    public function mount(): void
    {
        $this->updateData();
    }

    public function updateData(): void
    {
        $this->count_left = Holiday::where('leaves_at', '<', now())->count();
        $this->count_returned = Holiday::where('returns_at', '<', now())->count();
    }

    public function render()
    {
        $this->updateData();
        return view('livewire.holiday-count');
    }
}

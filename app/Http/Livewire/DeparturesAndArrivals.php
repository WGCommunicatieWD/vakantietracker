<?php

namespace App\Http\Livewire;

use App\Models\Holiday;
use Carbon\Carbon;
use Livewire\Component;

class DeparturesAndArrivals extends Component
{
    public $holidays;

    public function mount()
    {
        $this->holidays = collect();
    }

    public function updateData(): void
    {
        $start = Carbon::now()->startOfDay()->toDateTimeString();
        $end = Carbon::now()->endOfDay()->toDateTimeString();

        $departures = Holiday::whereBetween('leaves_at', [$start, $end])->orderBy('leaves_at', 'asc')->get();
        $departures = $departures->map(function ($departure) {
            $departure->timestamp = $departure->leaves_at;
            $departure->departs = true;
            return $departure;
        });
        $arrivals = Holiday::whereBetween('returns_at', [$start, $end])->orderBy('returns_at', 'asc')->get();
        $arrivals = $arrivals->map(function ($arrival) {
            $arrival->timestamp = $arrival->returns_at;
            $arrival->departs = false;
            return $arrival;
        });

        $this->holidays = $departures->concat($arrivals)->sortBy('timestamp');
    }

    public function render()
    {
        $this->updateData();
        return view('livewire.departures-and-arrivals');
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function __invoke(Request $request)
    {
        return view('welcome', [
            'show_facebook' => $request->has('facebook') ? boolval($request->get('facebook')) : true,
            'show_instagram' => $request->has('instagram') ? boolval($request->get('instagram')) : true,
        ]);
    }
}

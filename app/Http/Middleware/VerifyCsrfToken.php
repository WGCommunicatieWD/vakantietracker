<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        // These are safe components that do not store anything on the server
        // They have to be exempt from CSRF to be useable in iframes
        'livewire/message/guests-count/',
        'livewire/message/departures-and-arrivals/',
        'livewire/message/holiday-count/',
        'livewire/message/volunteer-count/',
    ];
}

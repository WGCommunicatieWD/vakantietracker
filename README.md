# Vakantietracker
Dit is een site om live te kunnen volgen welke vakanties van Kazou Waas & Dender 
vertrekken en toekomen, met foto's en allerlei leuke dingetjes.

## Opties
Je kan aan de URL de parameter `facebook=0` en/of `instagram=0` toevoegen om
respectievelijk de Facebook- en Instagramintegratie uit te schakelen.

Een link naar de pagina zonder socials ziet er dus als volgt uit:
`https://vakantietracker.be?facebook=0&instagram=0`.

## Installatie
We gaan er hier vanuit dat je een werkende PHP en MySQL installatie hebt
en Composer geinstalleerd hebt.

Download de repository, installeer de dependencies met 
`composer install --no-dev`. Kopieer dan het bestand `.env.example` naar `.env` 
en pas de database-gegevens aan. Voer ook het commando `php artisan key:generate`
uit om een geheime sleutel te genereren.

Je installeert de database met `php artisan migrate`.

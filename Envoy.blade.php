@servers(['prod' => ['kazouwd@185.182.59.5']])

@task('deploy', ['on' => ['prod'], 'parallel' => true])
cd /home/kazouwd/public_html/vakanties
php artisan down
git pull origin {{ $branch }}
~/.local/bin/composer install --no-dev -o
php artisan migrate --force
php artisan view:clear
php artisan cache:clear
php artisan optimize
php artisan up
@endtask
